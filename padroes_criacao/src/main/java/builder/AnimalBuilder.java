package builder;


import builder.data.BancoDeDados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AnimalBuilder {
    private Animal animal;
    Connection conexao = BancoDeDados.conectar();
    public AnimalBuilder() throws SQLException {
        animal = new Animal();
    }

    public Animal build() throws SQLException {
        if (animal.getReino() == null || animal.getReino().isEmpty()) {
            throw new IllegalArgumentException("Reino inválido");
        }
        if (animal.getFilo() == null || animal.getFilo().isEmpty()) {
            throw new IllegalArgumentException("Filo inválido");
        }
        if (animal.getClasse() == null || animal.getClasse().isEmpty()) {
            throw new IllegalArgumentException("Classe inválida");
        }
        if (animal.getOrdem() == null || animal.getOrdem().isEmpty()) {
            throw new IllegalArgumentException("Ordem inválida");
        }
        if (animal.getFamilia() == null || animal.getFamilia().isEmpty()) {
            throw new IllegalArgumentException("Família inválida");
        }
        if (animal.getGenero() == null || animal.getGenero().isEmpty()) {
            throw new IllegalArgumentException("Genêro inválido");
        }
        if (animal.getEspecie() == null || animal.getEspecie().isEmpty() || verificarEpecieExiste(animal.getEspecie())) {
            throw new IllegalArgumentException("Espécie inválida");
        }
        // Não há verificação de nome popular pelo fato de ser algo popular e não ter obrigatóriamente um nome e nem que seja unico
        return animal;
    }

    public AnimalBuilder setReino(String reino) {
        animal.setReino(reino);
        return this;
    }

    public AnimalBuilder setFilo(String filo) {
        animal.setFilo(filo);
        return this;
    }

    public AnimalBuilder setClasse(String classe) {
        animal.setClasse(classe);
        return this;
    }

    public AnimalBuilder setOrdem(String ordem) {
        animal.setOrdem(ordem);
        return this;
    }

    public AnimalBuilder setFamilia(String familia) {
        animal.setFamilia(familia);
        return this;
    }

    public AnimalBuilder setGenero(String genero) {
        animal.setGenero(genero);
        return this;
    }

    public AnimalBuilder setEspecie(String especie) {
        animal.setEspecie(especie);
        return this;
    }

    public AnimalBuilder setNmPopular(String nmPopular){
        animal.setNmPopular(nmPopular);
        return this;
    }

    private boolean verificarEpecieExiste(String especie) throws SQLException {

        String consulta = "SELECT * FROM classificacao WHERE especie = ?";

        try (PreparedStatement ps = conexao.prepareStatement(consulta)) {
            ps.setString(1, especie);

            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }
        }
    }
}
