package singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static DatabaseConnection databaseConnection;
    private Connection connection;

    private static final String URL = "jdbc:postgresql://localhost:5432/singleton";
    private static final String USER = "postgres";
    private static final String PASSWORD = "123456789";

    private DatabaseConnection() throws SQLException {
        try {
            connection = DriverManager.getConnection(URL,USER,PASSWORD);
        }catch (SQLException e){
            throw new SQLException("[ ERRO ] - Não foi possível conectar ao banco de dados, verifique se o banco está acessível!");
        }
    }

    public static DatabaseConnection getInstance() throws SQLException {
        if (databaseConnection == null){
            databaseConnection = new DatabaseConnection();
        }
        return databaseConnection;
    }

    public Connection getConnection(){
        return connection;
    }

    public void closeConnection() throws SQLException {
        if(connection != null){
            try {
                connection.close();
            }catch (SQLException e){
                throw new SQLException("[ ERRO ] - Não foi possível fechar a conexão!");
            }
        }
    }
}
