package prototype;

public class Montadora implements Cloneable {
    private String nome;
    private String local;
    private Carro carro;

    public Montadora(String nome, String local) {
        this.nome = nome;
        this.local = local;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public Carro criarModeloDeCarro(String modelo, String cor, String chassi, Short ano, Short motor, Roda roda){
        this.carro = new Carro(modelo,cor,motor,chassi,ano,roda);
        return carro;
    }

    @Override
    public Montadora clone() {
        try {
            Montadora clone = (Montadora) super.clone();
            clone.setCarro(this.carro.clone());
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}