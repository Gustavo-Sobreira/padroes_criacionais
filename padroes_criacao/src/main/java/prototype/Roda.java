package prototype;

public class Roda implements Cloneable {
    private String tipo;
    private Integer tamanho;

    public Roda(String tipo, Integer tamanho) {
        this.tamanho = tamanho;
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    @Override
    public Roda clone() {
        try {
            Roda rodaClone = (Roda) super.clone();
            return rodaClone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}