package prototype;

public class Carro implements Cloneable {
    private String modelo;
    private String cor;
    private Short ano;
    private Short motor;
    private String chassi;
    private Roda roda;

    public Carro(String modelo, String cor, Short motor, String chassi, Short ano, Roda roda) {
        this.ano = ano;
        this.cor = cor;
        this.chassi = chassi;
        this.modelo = modelo;
        this.motor = motor;
        this.roda = roda;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Short getAno() {
        return ano;
    }

    public void setAno(Short ano) {
        this.ano = ano;
    }

    public Short getMotor() {
        return motor;
    }

    public void setMotor(Short motor) {
        this.motor = motor;
    }

    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public Roda getRoda() {
        return roda;
    }

    public void setRoda(Roda roda) {
        this.roda = roda;
    }

    @Override
    public Carro clone() {
        try {
            Carro carroClone = (Carro) super.clone();
            carroClone.setRoda(this.roda.clone());
            return carroClone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}