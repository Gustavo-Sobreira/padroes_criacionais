package factoryMethod;

public class ArquivoPdf implements IArquivo{
    @Override
    public String setConteudo() {
        return "Conteúdo PDF";
    }

    @Override
    public String setNome() {
        return "Arquivo.pdf";
    }
}
