package factoryMethod;

public class ArquivoHtml implements IArquivo{
    @Override
    public String setConteudo() {
        return "<H1>html</H1>";
    }

    @Override
    public String setNome() {
        return "index.html";
    }
}
