package factoryMethod;

public interface IArquivo {
    public String setConteudo();
    public String setNome();
}