package factoryMethod;

public class ArquivoFactory {
    public static IArquivo alterarArquivo(String tipoDeArquivo){
        Class classe = null;
        Object objeto = null;
        try {
            classe = Class.forName("factoryMethod.Arquivo" + tipoDeArquivo);
            objeto = classe.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Tipo de arquivo não suportado");
        }
        if (!(objeto instanceof IArquivo)){
            throw new IllegalArgumentException("Ação inválida");
        }
        return (IArquivo) objeto;
    }
}