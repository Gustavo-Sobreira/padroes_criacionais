package abstractFactory;
public class HondaFactory implements MontadoraFactory {
    @Override
    public Carro montarCarro() {
        return new CarroHonda();
    }

    @Override
    public Moto montarMoto() {
        return new MotoHonda();
    }
}
