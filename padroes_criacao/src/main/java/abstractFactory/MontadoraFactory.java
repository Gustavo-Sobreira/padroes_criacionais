package abstractFactory;

public interface MontadoraFactory {
    Carro montarCarro();
    Moto montarMoto();
}
