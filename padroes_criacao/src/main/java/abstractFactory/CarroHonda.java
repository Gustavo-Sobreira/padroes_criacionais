package abstractFactory;
public class CarroHonda implements Carro {
    @Override
    public String acelerar() {
        return "Carro Honda acelerando...";
    }

    @Override
    public String frear() {
        return "Carro Honda freando...";
    }
}