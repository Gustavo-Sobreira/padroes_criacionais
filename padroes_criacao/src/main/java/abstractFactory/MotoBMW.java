package abstractFactory;

public class MotoBMW implements Moto {
    @Override
    public String acelerar() {
        return "Moto BMW acelerando...";
    }

    @Override
    public String frear() {
        return "Moto BMW freando...";
    }
}