package abstractFactory;

public interface Moto {

    String acelerar();

    String frear();
}
