package abstractFactory;

public class MotoHonda implements Moto {
    @Override
    public String acelerar() {
       return "Moto Honda acelerando...";
    }

    @Override
    public String frear() {
        return "Moto Honda freando...";
    }
}