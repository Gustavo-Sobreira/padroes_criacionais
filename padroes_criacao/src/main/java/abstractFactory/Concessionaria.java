package abstractFactory;

public class Concessionaria {
    private Carro carro;
    private  Moto moto;

    public Concessionaria(MontadoraFactory montadora){
        this.moto = montadora.montarMoto();
        this.carro = montadora.montarCarro();
    }

    public String acelerarMoto(){
        return moto.acelerar();
    }
    public String acelerarCarro(){
        return carro.acelerar();
    }
    public String frearMoto(){
        return moto.frear();
    }

    public String frearCarro(){
        return carro.frear();
    }
}
