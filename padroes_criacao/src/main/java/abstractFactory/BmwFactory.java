package abstractFactory;

public class BmwFactory implements MontadoraFactory {
    @Override
    public Carro montarCarro() {
        return new CarroBMW();
    }

    @Override
    public Moto montarMoto() {
        return new MotoBMW();
    }
}
