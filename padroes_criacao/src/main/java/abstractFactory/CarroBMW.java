package abstractFactory;

public class CarroBMW implements Carro {
    @Override
    public String acelerar() {
        return "Carro BMW acelerando...";
    }

    @Override
    public String frear() {
        return "Carro BMW freando...";
    }
}