<h1 align="center">Singleton</h1>

***


### Escopo: Objetos
### Propósito: Criação
“Garantir que uma classe tenha somente uma instância e fornecer um ponto global de
acesso para a mesma." (Gamma,2000, p.130)
***
# ⚠️ Problema
### Várias conexões com banco
Em uma empresa de software foi desenvolvido uma solução que em fase de testes não apresentou nenhum defeito em relação
a quantidade e de conexões simultâneas porém ao liberar ao público a solução travou, resolva. 

***
# 📐 Modelo
![img.png](img.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesCriacao.singleton**
- Pacote de Testes: está localizado em: **src.test.java.padroesCriacao.singleton**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








