<h1 align="center">Builder</h1>

***


### Escopo: Objetos
### Propósito: Criação
"Separar a construção de um objeto complexo da sua representação de modo que o
mesmo processo de construção possa criar diferentes representações." (Gamma,2000, p.104).
***
# ⚠️ Problema
### Cadastrar novas espécies

- O processo de classificação de um novo animal, é burocrático, necessita do estudo aprofundado de uma série de fatores,
como reino, família, etc. 
- Crie um programa que permita cadastrar diferentes classificações para animais.

***
# 📐 Modelo
![img.png](img.png)
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesCriacao.builder**
- Pacote de Testes: está localizado em: **src.test.java.padroesCriacao.builder**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Vá em **src/docker/padroesCriaco/builder** e rode o seguinte comando:
        
    docker compose up -d

4° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








