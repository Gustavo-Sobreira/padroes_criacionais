package singleton;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;



public class DatabaseConnectionTest {
    private DatabaseConnection databaseConnection;

    @BeforeEach
    public void setUp() throws SQLException {
        databaseConnection = DatabaseConnection.getInstance();
    }
    @Test
    public void deveRetornarMesmaInstancia() throws SQLException {
        DatabaseConnection anotherDatabaseConnection;
        anotherDatabaseConnection = DatabaseConnection.getInstance();
        assertSame(databaseConnection, anotherDatabaseConnection);
    }

    @Test
    public void deveRetornarConexaoNaoNula() {
        Connection connection = databaseConnection.getConnection();
        assertNotNull(connection);
    }

    @After
    public void tearDown() throws SQLException {
        databaseConnection.closeConnection();
    }
}
