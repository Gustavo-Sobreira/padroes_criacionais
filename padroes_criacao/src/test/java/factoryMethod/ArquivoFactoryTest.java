package factoryMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ArquivoFactoryTest {
    @Test
    void deveRetornarExcessaoParaTipoNaoExistente(){
        try{
            IArquivo arquivo = ArquivoFactory.alterarArquivo("XML");
            fail();
        }catch (IllegalArgumentException e){
            assertEquals("Tipo de arquivo não suportado", e.getMessage());
        }
    }

    @Test
    void deveRetornarExcessaoParaAcaoInvalida(){
        try{
            IArquivo arquivo = ArquivoFactory.alterarArquivo("Doc");
            fail();
        }catch (IllegalArgumentException e){
            assertEquals("Ação inválida", e.getMessage());
        }
    }
}
