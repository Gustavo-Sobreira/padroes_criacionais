package factoryMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class ArquivoDocTest {
    @Test
    public void deveRetornarAcaoInvalida(){
        try {
            IArquivo arquivo = ArquivoFactory.alterarArquivo("Doc");
            fail();
        }catch (Exception e){
            assertEquals("Ação inválida", e.getMessage());
        }
    }
}
