package factoryMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArquivoHtmlTest {
    @Test
    public void deveInserirConteudoNoArquivo(){
        IArquivo arquivo = ArquivoFactory.alterarArquivo("Html");
        assertEquals("<H1>html</H1>", arquivo.setConteudo());
    }

    @Test
    public void deveInserirNomeNoArquivo(){
        IArquivo arquivo = ArquivoFactory.alterarArquivo("Html");
        assertEquals("index.html", arquivo.setNome());
    }
}
