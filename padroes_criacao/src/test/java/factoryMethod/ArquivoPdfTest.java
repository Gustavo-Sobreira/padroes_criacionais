package factoryMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArquivoPdfTest {
    @Test
    public void deveInserirConteudoNoArquivo(){
        IArquivo arquivo = ArquivoFactory.alterarArquivo("Pdf");
        assertEquals("Conteúdo PDF", arquivo.setConteudo());
    }

    @Test
    public void deveInserirNomeNoArquivo(){
        IArquivo arquivo = ArquivoFactory.alterarArquivo("Pdf");
        assertEquals("Arquivo.pdf", arquivo.setNome());
    }
}
