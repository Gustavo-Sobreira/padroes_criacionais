package abstractFactory;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDrive {
    @Test
    public void deveAcelerarComCarroHonda(){
        Concessionaria concessionaria = new Concessionaria(new HondaFactory());
        assertEquals("Carro Honda acelerando...", concessionaria.acelerarCarro());
    }
    @Test
    public void deveFrearComCarroHonda(){
        Concessionaria concessionaria = new Concessionaria(new HondaFactory());
        assertEquals("Carro Honda freando...", concessionaria.frearCarro());
    }

    @Test
    public void deveAcelerarComMotoHonda(){
        Concessionaria concessionaria = new Concessionaria(new HondaFactory());
        assertEquals("Moto Honda acelerando...", concessionaria.acelerarMoto());
    }
    @Test
    public void deveFrearComMotoHonda(){
        Concessionaria concessionaria = new Concessionaria(new HondaFactory());
        assertEquals("Moto Honda freando...", concessionaria.frearMoto());
    }

    @Test
    public void deveAcelerarComCarroBmw(){
        Concessionaria concessionaria = new Concessionaria(new BmwFactory());
        assertEquals("Carro BMW acelerando...", concessionaria.acelerarCarro());
    }
    @Test
    public void deveFrearComCarroBmw(){
        Concessionaria concessionaria = new Concessionaria(new BmwFactory());
        assertEquals("Carro BMW freando...", concessionaria.frearCarro());
    }

    @Test
    public void deveAcelerarComMotoBmw(){
        Concessionaria concessionaria = new Concessionaria(new BmwFactory());
        assertEquals("Moto BMW acelerando...", concessionaria.acelerarMoto());
    }
    @Test
    public void deveFrearComMotoBmw(){
        Concessionaria concessionaria = new Concessionaria(new BmwFactory());
        assertEquals("Moto BMW freando...", concessionaria.frearMoto());
    }
}