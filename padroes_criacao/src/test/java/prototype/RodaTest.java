package prototype;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class RodaTest {
    @Test
    public void deveTerEnderecoDeMemoriaDiferentes(){
        Roda rodaOriginal = new Roda("Liga-leve", 20);
        Roda rodaClone = rodaOriginal.clone();
        assertNotEquals(rodaOriginal.hashCode(), rodaClone.hashCode());
    }

    @Test
    public void deveTerTipoIgual(){
        Roda rodaOriginal = new Roda("Liga-leve", 20);
        Roda rodaClone = rodaOriginal.clone();
        assertEquals(rodaOriginal.getTipo(), rodaClone.getTipo());
    }

    @Test
    public void deveTerTipoDiferente(){
        Roda rodaOriginal = new Roda("Liga-leve", 20);
        Roda rodaClone = rodaOriginal.clone();
        rodaClone.setTipo("Cromada");
        assertNotEquals(rodaOriginal.getTipo(), rodaClone.getTipo());
    }

    @Test
    public void deveTerTamanhoIgual(){
        Roda rodaOriginal = new Roda("Liga-leve", 20);
        Roda rodaClone = rodaOriginal.clone();
        assertEquals(rodaOriginal.getTamanho(), rodaClone.getTamanho());
    }

    @Test
    public void deveTerTamanhoDiferente(){
        Roda rodaOriginal = new Roda("Liga-leve", 20);
        Roda rodaClone = rodaOriginal.clone();
        rodaClone.setTamanho(18);
        assertNotEquals(rodaOriginal.getTamanho(), rodaClone.getTamanho());
    }
}
