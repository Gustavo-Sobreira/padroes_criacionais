package prototype;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class CarroTest {
    @Test
    public void deveTerEnderecoDeMemoriaDiferentes(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertNotEquals(carroOriginal.hashCode(), carroClone.hashCode());
    }

    @Test
    public void deveTerModeloIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getModelo(), carroClone.getModelo());
    }

    @Test
    public void deveTerModeloDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setModelo("Ka");
        assertNotEquals(carroOriginal.getModelo(), carroClone.getModelo());
    }

    @Test
    public void deveTerCorIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getCor(), carroClone.getCor());
    }

    @Test
    public void deveTerCorDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setCor("Prata");
        assertNotEquals(carroOriginal.getCor(), carroClone.getCor());
    }

    @Test
    public void deveTerMotorIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getMotor(), carroClone.getMotor());
    }

    @Test
    public void deveTerMotorDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setMotor((short)1.0);
        assertNotEquals(carroOriginal.getMotor(), carroClone.getMotor());
    }

    @Test
    public void deveTerChassiIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getChassi(), carroClone.getChassi());
    }

    @Test
    public void deveTerChassiDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setChassi("21da5s4d6asd");
        assertNotEquals(carroOriginal.getChassi(), carroClone.getChassi());
    }

    @Test
    public void deveTerAnoIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getAno(), carroClone.getAno());
    }

    @Test
    public void deveTerAnoDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setAno((short) 2005);
        assertNotEquals(carroOriginal.getAno(), carroClone.getAno());
    }

    @Test
    public void deveTerRodaIgual(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        assertEquals(carroOriginal.getRoda().getTipo(), carroClone.getRoda().getTipo());
    }

    @Test
    public void deveTerRodaDiferente(){
        Carro carroOriginal = new Carro("Ranger","branca", (short) 2.8,"a5d416as5fd", (short) 2019, new Roda("Liga-leve", 22));
        Carro carroClone = carroOriginal.clone();
        carroClone.setRoda(new Roda("Aluminio", 15));
        assertNotEquals(carroOriginal.getRoda(), carroClone.getRoda());
    }
}
