package prototype;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MontadoraTest {
    Roda roda = new Roda("alumínio", 18);;
    Montadora montadoraOriginal = new Montadora("Ford", "Brasil");
    Carro carro = montadoraOriginal.criarModeloDeCarro("Fiesta", "vermelho", "19-Xajbreud215aef56", (short) 2024, (short) 2, roda);

    Montadora segundaMontadora = montadoraOriginal.clone();

    @Test
    public void deveAlterarNomeMontadora() {
        assertEquals(segundaMontadora.getNome(), montadoraOriginal.getNome());

        segundaMontadora.setNome("Fiat");
        assertNotEquals(segundaMontadora.getNome(),montadoraOriginal.getNome());
    }

    @Test
    public void deveAlterarLocalMontadora() {
        assertEquals(segundaMontadora.getLocal(), montadoraOriginal.getLocal());

        segundaMontadora.setLocal("França");
        assertNotEquals(segundaMontadora.getLocal(),montadoraOriginal.getLocal());
    }

    @Test
    public void deveCriarNovoCarro(){
        Carro ranger = montadoraOriginal.criarModeloDeCarro("Ranger", "preta", "19-Xajbreud215aef56", (short) 1999, (short) 2.4, roda);
        assertNotEquals(ranger, carro);
    }

    @Test
    public void deveCompararCarroDaMontadoraOriginalComClone(){
        Carro ranger = montadoraOriginal.criarModeloDeCarro("Ranger", "preta", "19-Xajbreud215aef56", (short) 1999, (short) 2.4, roda);
        assertNotEquals(montadoraOriginal.getCarro(), segundaMontadora.getCarro());
    }
}
