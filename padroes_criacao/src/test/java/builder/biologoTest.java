package builder;


import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class biologoTest {

    @Test
    public void deveRetornarExcecaoPorReinoVazio(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Reino inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorReinoNulo(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Reino inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorFiloVazio(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Filo inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorFiloNull(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Filo inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorClasseVazia(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Classe inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorClasseNula(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Classe inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorOrdemVazia(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Ordem inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorOrdemNula(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Ordem inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorFamiliaVazia(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Família inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorFamiliaNula(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Família inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorGeneroVazio(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Genêro inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorGeneroNulo(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Genêro inválido", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void deveRetornarExcecaoPorEspecieExistente(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("angustula")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Espécie inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorEspecieVazia(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setEspecie("")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Espécie inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarExcecaoPorEspecieNula(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Tetragonisca")
                    .setNmPopular("Abelha-jatai")
                    .build();
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Espécie inválida", e.getMessage());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void deveRetornarUmAnimal(){
        try {
            AnimalBuilder animalBuilder = new AnimalBuilder();
            Animal animal = animalBuilder
                    .setReino("Animalia")
                    .setFilo("Arthropoda")
                    .setClasse("Insecta")
                    .setOrdem("Hymenoptera")
                    .setFamilia("Apidae")
                    .setGenero("Melipona")
                    .setEspecie("quadrifasciata")
                    .setNmPopular("Abelha-mandacaia")
                    .build();

            assertNotNull(animal);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
