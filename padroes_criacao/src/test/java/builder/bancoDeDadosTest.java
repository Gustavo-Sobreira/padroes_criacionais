package builder;


import builder.data.BancoDeDados;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;

public class bancoDeDadosTest {
    @Test
    public void casoBancoEstejaDesligadoRetornaErro(){
        Connection connection = BancoDeDados.conectar();
        if (connection == null){
            assertNull(connection);
            fail();
        }else {
            assertNotNull(connection);
        }
    }
}
