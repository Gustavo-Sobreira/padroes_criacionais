<h1 align="center">Composite</h1>

***

### Escopo: Objetos
### Propósito: Estrutural
"Compor objetos em estruturas de árvore para representarem hierarquias partes-todo.
Composite permite aos clientes tratarem de maneira uniforme objetos individuais e
composições de objetos." (Gamma,2000, p.160).

# ⚠️ Problema
### Padronizar entregas de páscoa
Crie um gerenciador de tarefas onde uma tarefa simples seja completa por si só e tarefas compostas
sejam a junção de tarefas simples.
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








