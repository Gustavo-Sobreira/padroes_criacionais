<h1 align="center">Proxy</h1>

***

### Escopo: Objetos
### Propósito: Estrutural
"Fornece um substituto (surrogate) ou marcador da localização de outro objeto para
controlar o acesso a esse objeto." (Gamma,2000, p.198).

# ⚠️ Problema
### Padronizar entregas de páscoa
Faça o proxy para notas de alunos.  
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








