<h1 align="center">Flyweight</h1>

***

### Escopo: Objetos
### Propósito: Estrutural
"Usar compartilhamento para suportar eficientemente grandes quantidades de obje-
tos de granularidade fina." (Gamma,2000, p.187).

# ⚠️ Problema
### Padronizar entregas de páscoa
Crie um projeto que possa armazenar e separar características de bovinos.  
***
# 📐 Modelo
*acesse o modelo em: https://drive.google.com/file/d/1TOzU5boBwZKl1e3iQDBSjwJgJmtiKTs-/view?usp=sharing*

***
# 🏗️ Estrutura
O projeto se divide em duas partes:
- Pacote Principal: está localizado em: **src.main.java.padroesEstruturais.adapter**
- Pacote de Testes: está localizado em: **src.test.java.padroesEstruturais.adapter**

***
# ▶️ Iniciar projeto
1° - Clone o projeto

    git clone https://gitlab.com/Gustavo-Sobreira/padroes_de_projetos

2° - Importe o projeto para a sua IDE.

3° - Execute o arquivo presente no pacote de testes, para verificar o funcionamento do código.








