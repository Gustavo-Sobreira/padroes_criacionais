package bridge;

public class Mago implements IClasse{

    @Override
    public Double valorDeAtaque() {
        return 9.0;
    }

    @Override
    public Double valorDeDefesa() {
        return 1.0;
    }

    @Override
    public Integer valorMultiplicadorBonusAtaque() {
        return 5;
    }

    @Override
    public Integer valorMultiplicadorBonusDefesa() {
        return 1;
    }
}
