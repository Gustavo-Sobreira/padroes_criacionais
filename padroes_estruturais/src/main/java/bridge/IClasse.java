package bridge;

public interface IClasse {
    public Double valorDeAtaque();
    public Double valorDeDefesa();
    public Integer valorMultiplicadorBonusAtaque();
    public Integer valorMultiplicadorBonusDefesa();
}
