package bridge;

public class Boss extends Avatar{
    public Boss(Integer nivel, IClasse classe) {
        super(nivel, classe);
    }

    @Override
    public void setAtaque() {
        this.ataque = classe.valorDeAtaque() + (classe.valorMultiplicadorBonusAtaque() * (nivel + 5));
    }

    @Override
    public void setDefesa() {
        this.defesa = classe.valorDeDefesa() + (classe.valorMultiplicadorBonusDefesa() * (nivel + 5));
    }

}
