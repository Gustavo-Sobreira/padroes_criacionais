package bridge;

public class Paladino implements IClasse{
    @Override
    public Double valorDeAtaque() {
        return 5.0;
    }

    @Override
    public Double valorDeDefesa() {
        return 5.0;
    }

    @Override
    public Integer valorMultiplicadorBonusAtaque() {
        return 3;
    }

    @Override
    public Integer valorMultiplicadorBonusDefesa() {
        return 7;
    }
}
