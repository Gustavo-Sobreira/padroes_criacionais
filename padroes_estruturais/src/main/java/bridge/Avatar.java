package bridge;

public abstract class Avatar{
    protected IClasse classe;
    protected Double ataque;
    protected Double defesa;
    protected Integer nivel;
    public Avatar(Integer nivel, IClasse classe){
        this.classe = classe;
        this.nivel = nivel;
    }
    public Double getAtaque(){
        return ataque  ;
    }
    public Double getDefesa(){
        return defesa;
    }
    public void setAtaque(){
        this.ataque = classe.valorDeAtaque() + (classe.valorMultiplicadorBonusAtaque() * nivel);
    }
    public void setDefesa(){
        this.defesa = classe.valorDeDefesa() + (classe.valorMultiplicadorBonusDefesa() * nivel);
    }

}