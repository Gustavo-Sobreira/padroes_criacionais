package bridge;

public class Arqueiro implements IClasse{
    @Override
    public Double valorDeAtaque() {
        return 7.0;
    }

    @Override
    public Double valorDeDefesa() {
        return 3.0;
    }

    @Override
    public Integer valorMultiplicadorBonusAtaque() {
        return 4;
    }

    @Override
    public Integer valorMultiplicadorBonusDefesa() {
        return 6;
    }
}
