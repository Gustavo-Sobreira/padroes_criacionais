package decorator;

public abstract class AtaqueDecorator implements IAtaque {
    protected IAtaque ataque;

    public AtaqueDecorator(IAtaque ataque) {
        this.ataque = ataque;
    }

    @Override
    public Integer getTotalDeDano() {
        return ataque.getTotalDeDano();
    }

    @Override
    public String getArmaUtilizada() {
        return ataque.getArmaUtilizada();
    }
}