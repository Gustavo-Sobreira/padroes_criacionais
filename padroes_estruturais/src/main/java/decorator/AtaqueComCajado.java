package decorator;

public class AtaqueComCajado extends AtaqueDecorator {

    public AtaqueComCajado(IAtaque ataque) {
        super(ataque);
    }

    @Override
    public Integer getTotalDeDano() {
        return super.getTotalDeDano() + 3;
    }

    @Override
    public String getArmaUtilizada() {
        return super.getArmaUtilizada() + ", Cajado";
    }
}