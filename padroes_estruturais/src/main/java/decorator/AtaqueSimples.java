package decorator;

public class AtaqueSimples implements IAtaque {

    @Override
    public Integer getTotalDeDano() {
        return 1;
    }

    @Override
    public String getArmaUtilizada() {
        return "Ataque Simples";
    }
}
