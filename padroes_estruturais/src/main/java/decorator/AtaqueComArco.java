package decorator;

public class AtaqueComArco extends AtaqueDecorator {

    public AtaqueComArco(IAtaque ataque) {
        super(ataque);
    }

    @Override
    public Integer getTotalDeDano() {
        return super.getTotalDeDano() + 2;
    }

    @Override
    public String getArmaUtilizada() {
        return super.getArmaUtilizada() + ", Arco";
    }
}
