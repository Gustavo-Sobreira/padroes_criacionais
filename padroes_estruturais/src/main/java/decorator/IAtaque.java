package decorator;

public interface IAtaque {
    public Integer getTotalDeDano();
    public String getArmaUtilizada();
}
