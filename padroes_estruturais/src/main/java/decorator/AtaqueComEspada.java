package decorator;

public class AtaqueComEspada extends AtaqueDecorator {

    public AtaqueComEspada(IAtaque ataque) {
        super(ataque);
    }

    @Override
    public Integer getTotalDeDano() {
        return super.getTotalDeDano() + 5;
    }

    @Override
    public String getArmaUtilizada() {
        return super.getArmaUtilizada() + ", Espada";
    }
}