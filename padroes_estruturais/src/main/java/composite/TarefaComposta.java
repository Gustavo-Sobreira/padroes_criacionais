package composite;

import java.util.ArrayList;
import java.util.List;

public class TarefaComposta implements Tarefa {
    private String nome;
    private List<Tarefa> subTarefas = new ArrayList<>();

    public TarefaComposta(String nome) {
        this.nome = nome;
    }

    public void adicionarTarefa(Tarefa tarefa) {
        subTarefas.add(tarefa);
    }

    public void removerTarefa(Tarefa tarefa) {
        subTarefas.remove(tarefa);
    }

    @Override
    public void realizar() {
        System.out.println("Realizando a tarefa composta: " + nome);
        for (Tarefa tarefa : subTarefas) {
            tarefa.realizar();
        }
    }

    @Override
    public String toString() {
        return nome + " com sub-tarefas: " + subTarefas;
    }
}
