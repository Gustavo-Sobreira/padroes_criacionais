package composite;

public class TarefaSimples implements Tarefa {
    private String nome;

    public TarefaSimples(String nome) {
        this.nome = nome;
    }

    @Override
    public void realizar() {
        System.out.println("Realizando a tarefa: " + nome);
    }

    @Override
    public String toString() {
        return nome;
    }
}
