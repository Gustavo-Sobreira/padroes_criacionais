package flyweight;


public class BoiConcreto implements Boi {
    private final String raca;
    private final String cor;

    public BoiConcreto(String raca, String cor) {
        this.raca = raca;
        this.cor = cor;
    }

    @Override
    public void mostrarDetalhes() {
        System.out.println("Boi da raça: " + raca + ", cor: " + cor);
    }

    public String getRaca() {
        return raca;
    }

    public String getCor() {
        return cor;
    }
}
