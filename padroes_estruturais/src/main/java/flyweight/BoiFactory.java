package flyweight;


import java.util.HashMap;
import java.util.Map;

public class BoiFactory {
    private static final Map<String, Boi> bois = new HashMap<>();

    public static Boi getBoi(String raca, String cor) {
        String chave = raca + "-" + cor;
        if (!bois.containsKey(chave)) {
            bois.put(chave, new BoiConcreto(raca, cor));
        }
        return bois.get(chave);
    }

    public static int getTotalBois() {
        return bois.size();
    }
}
