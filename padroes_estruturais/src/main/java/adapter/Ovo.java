package adapter;

public class Ovo {

    IOvoMedida medida;
    OvoAdpter adpter;

    public Ovo(){
        medida = new OvoMedida();
        adpter = new OvoAdpter(medida);
    }

    public void setMedida(String ovoMedida){
        medida.setMedida(ovoMedida);
        adpter.salvarMEdida();
    }

    public String getMedida(){
        return adpter.recuperarMedida();
    }

    public Integer getPeso(){
        return adpter.getPesoEmGrama();
    }
}
