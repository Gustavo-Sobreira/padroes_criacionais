package adapter;


public class OvoPeso {
    private Integer pesoEmGrama;

    public Integer getPesoEmGrama(){
        return pesoEmGrama;
    }

    public void setPesoEmGrama(Integer peso){
        this.pesoEmGrama = peso;
    }
}
