package adapter;


public class OvoAdpter extends OvoPeso{

    private IOvoMedida ovoMedida;

    public OvoAdpter(IOvoMedida ovoMedida) {
        this.ovoMedida = ovoMedida;
    }

    public String recuperarMedida() {
        if (this.getPesoEmGrama().equals(1000)){
            ovoMedida.setMedida("Grande");
        } else if (this.getPesoEmGrama().equals(500)) {
            ovoMedida.setMedida("Médio");
        } else {
            ovoMedida.setMedida("Pequeno");
        }
        return ovoMedida.getMedida();
    }

    public void salvarMEdida() {
        if (ovoMedida.getMedida().equals("Grande")){
            this.setPesoEmGrama(1000);
        } else if (ovoMedida.getMedida().equals("Médio")) {
            this.setPesoEmGrama(500);
        }else {
            this.setPesoEmGrama(250);
        }
    }
}
