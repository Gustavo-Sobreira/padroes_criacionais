package adapter;

public interface IOvoMedida {

    String getMedida();

    void setMedida(String medida);
}
