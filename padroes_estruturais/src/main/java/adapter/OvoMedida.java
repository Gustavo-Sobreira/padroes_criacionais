package adapter;

public class OvoMedida implements IOvoMedida{
    private String medida;
    @Override
    public String getMedida() {
        return medida;
    }

    @Override
    public void setMedida(String medida) {
        this.medida = medida;
    }
}
