package facade;

public class Som extends Dispositivo {
    private static Som somInstance;

    private Som() {}

    public static Som getSomInstance() {
        if (somInstance == null) {
            somInstance = new Som();
        }
        return somInstance;
    }
}
