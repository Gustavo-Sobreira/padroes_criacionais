package facade;

public class ControleDispositivosFacade {

    public static Boolean ligarCasa() {
        return Luz.getLuzInstance().ligar() &&
                ArCondicionado.getArCondicionado().ligar() &&
                Som.getSomInstance().ligar();
    }

    public static Boolean desligarCasa() {
        return Luz.getLuzInstance().desligar() &&
                ArCondicionado.getArCondicionado().desligar() &&
                Som.getSomInstance().desligar();
    }
}
