package facade;

public class ArCondicionado extends Dispositivo {
    private static ArCondicionado arCondicionado;

    private ArCondicionado() {}

    public static ArCondicionado getArCondicionado() {
        if (arCondicionado == null) {
            arCondicionado = new ArCondicionado();
        }
        return arCondicionado;
    }
}
