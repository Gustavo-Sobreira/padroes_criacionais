package facade;

public class Luz extends Dispositivo {
    private static Luz luzInstance;

    private Luz() {}

    public static Luz getLuzInstance() {
        if (luzInstance == null) {
            luzInstance = new Luz();
        }
        return luzInstance;
    }
}
