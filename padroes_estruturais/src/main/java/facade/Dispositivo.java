package facade;

public abstract class Dispositivo {

    public Boolean ligar() {
        return true;
    }

    public Boolean desligar() {
        return false;
    }
}
