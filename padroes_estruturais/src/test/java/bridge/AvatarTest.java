package bridge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AvatarTest {

    private IClasse arqueiro;
    private IClasse mago;
    private IClasse paladino;

    @BeforeEach
    public void setUp() {
        arqueiro = new Arqueiro();
        mago = new Mago();
        paladino = new Paladino();
    }

    @Test
    public void testJogadorComArqueiro() {
        Avatar jogador = new Jogador(1, arqueiro);
        jogador.setAtaque();
        jogador.setDefesa();
        assertEquals(11.0, jogador.getAtaque());
        assertEquals(9.0, jogador.getDefesa());
    }

    @Test
    public void testJogadorComMago() {
        Avatar jogador = new Jogador(1, mago);
        jogador.setAtaque();
        jogador.setDefesa();
        assertEquals(14.0, jogador.getAtaque());
        assertEquals(2.0, jogador.getDefesa());
    }

    @Test
    public void testJogadorComPaladino() {
        Avatar jogador = new Jogador(1, paladino);
        jogador.setAtaque();
        jogador.setDefesa();
        assertEquals(8.0, jogador.getAtaque());
        assertEquals(12.0, jogador.getDefesa());
    }

    @Test
    public void testBossComArqueiro() {
        Avatar boss = new Boss(1, arqueiro);
        boss.setAtaque();
        boss.setDefesa();
        assertEquals(31.0, boss.getAtaque());
        assertEquals(39.0, boss.getDefesa());
    }

    @Test
    public void testBossComMago() {
        Avatar boss = new Boss(1, mago);
        boss.setAtaque();
        boss.setDefesa();
        assertEquals(39.0, boss.getAtaque());
        assertEquals(7.0, boss.getDefesa());
    }

    @Test
    public void testBossComPaladino() {
        Avatar boss = new Boss(1, paladino);
        boss.setAtaque();
        boss.setDefesa();
        assertEquals(23.0, boss.getAtaque());
        assertEquals(47.0, boss.getDefesa());
    }

    @Test
    public void testNpcComArqueiro() {
        Avatar npc = new Npc(1, arqueiro);
        npc.setAtaque();
        npc.setDefesa();
        assertEquals(16.0, npc.getAtaque());
        assertEquals(14.0, npc.getDefesa());
    }

    @Test
    public void testNpcComMago() {
        Avatar npc = new Npc(1, mago);
        npc.setAtaque();
        npc.setDefesa();
        assertEquals(19.0, npc.getAtaque());
        assertEquals(7.0, npc.getDefesa());
    }

    @Test
    public void testNpcComPaladino() {
        Avatar npc = new Npc(1, paladino);
        npc.setAtaque();
        npc.setDefesa();
        assertEquals(13.0, npc.getAtaque());
        assertEquals(17.0, npc.getDefesa());
    }

    @Test
    public void testValoresDeClasseArqueiro() {
        assertEquals(7.0, arqueiro.valorDeAtaque());
        assertEquals(3.0, arqueiro.valorDeDefesa());
        assertEquals(4, arqueiro.valorMultiplicadorBonusAtaque());
        assertEquals(6, arqueiro.valorMultiplicadorBonusDefesa());
    }

    @Test
    public void testValoresDeClasseMago() {
        assertEquals(9.0, mago.valorDeAtaque());
        assertEquals(1.0, mago.valorDeDefesa());
        assertEquals(5, mago.valorMultiplicadorBonusAtaque());
        assertEquals(1, mago.valorMultiplicadorBonusDefesa());
    }

    @Test
    public void testValoresDeClassePaladino() {
        assertEquals(5.0, paladino.valorDeAtaque());
        assertEquals(5.0, paladino.valorDeDefesa());
        assertEquals(3, paladino.valorMultiplicadorBonusAtaque());
        assertEquals(7, paladino.valorMultiplicadorBonusDefesa());
    }
}
