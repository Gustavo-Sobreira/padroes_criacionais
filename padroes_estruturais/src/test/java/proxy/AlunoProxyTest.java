package proxy;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AlunoProxyTest {

    @BeforeAll
    public static void setup() {
        Aluno aluno1 = new Aluno(1, "João", "São Paulo", 8.5f, 7.0f);
        Aluno aluno2 = new Aluno(2, "Maria", "Rio de Janeiro", 9.0f, 8.0f);
        BD.addAluno(aluno1);
        BD.addAluno(aluno2);
    }

    @Test
    public void testObterDadosPessoais() {
        IAluno alunoProxy = new AlunoProxy(1);
        List<String> dadosPessoais = alunoProxy.obterDadosPessoais();
        assertEquals(Arrays.asList("João", "São Paulo"), dadosPessoais);
    }

    @Test
    public void testObterNotasAutorizado() {
        IAluno alunoProxy = new AlunoProxy(2);
        Instrutor instrutor = new Instrutor("Professor X", true);
        List<Float> notas = alunoProxy.obterNotas(instrutor);
        assertEquals(Arrays.asList(9.0f, 8.0f), notas);
    }

    @Test
    public void testObterNotasNaoAutorizado() {
        IAluno alunoProxy = new AlunoProxy(1);
        Instrutor instrutor = new Instrutor("Professor Y", false);
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            alunoProxy.obterNotas(instrutor);
        });
        assertEquals("Funcionário não autorizado", exception.getMessage());
    }

    @Test
    public void testAlunoSingleton() {
        Aluno aluno1 = BD.getAluno(1);
        Aluno aluno2 = new Aluno(1);
        assertNotEquals(aluno1, aluno2);
    }
}
