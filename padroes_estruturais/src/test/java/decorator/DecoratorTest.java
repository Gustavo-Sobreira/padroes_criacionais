package decorator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DecoratorTest {

    @Test
    public void testAtaqueSimples() {
        IAtaque ataque = new AtaqueSimples();
        assertEquals(1, ataque.getTotalDeDano());
        assertEquals("Ataque Simples", ataque.getArmaUtilizada());
    }

    @Test
    public void testAtaqueComEspada() {
        IAtaque ataque = new AtaqueComEspada(new AtaqueSimples());
        assertEquals(6, ataque.getTotalDeDano());
        assertEquals("Ataque Simples, Espada", ataque.getArmaUtilizada());
    }

    @Test
    public void testAtaqueComCajado() {
        IAtaque ataque = new AtaqueComCajado(new AtaqueSimples());
        assertEquals(4, ataque.getTotalDeDano());
        assertEquals("Ataque Simples, Cajado", ataque.getArmaUtilizada());
    }

    @Test
    public void testAtaqueComArco() {
        IAtaque ataque = new AtaqueComArco(new AtaqueSimples());
        assertEquals(3, ataque.getTotalDeDano());
        assertEquals("Ataque Simples, Arco", ataque.getArmaUtilizada());
    }

    @Test
    public void testAtaqueComEspadaECajado() {
        IAtaque ataque = new AtaqueComCajado(new AtaqueComEspada(new AtaqueSimples()));
        assertEquals(9, ataque.getTotalDeDano());
        assertEquals("Ataque Simples, Espada, Cajado", ataque.getArmaUtilizada());
    }

    @Test
    public void testAtaqueComEspadaCajadoEArco() {
        IAtaque ataque = new AtaqueComArco(new AtaqueComCajado(new AtaqueComEspada(new AtaqueSimples())));
        assertEquals(11, ataque.getTotalDeDano());
        assertEquals("Ataque Simples, Espada, Cajado, Arco", ataque.getArmaUtilizada());
    }
}
