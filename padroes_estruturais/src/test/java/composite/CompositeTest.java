package composite;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

public class CompositeTest {

    private Tarefa tarefa1;
    private Tarefa tarefa2;
    private Tarefa tarefa3;
    private TarefaComposta tarefaComposta1;
    private TarefaComposta tarefaComposta2;

    @BeforeEach
    public void setUp() {
        tarefa1 = new TarefaSimples("Comprar materiais");
        tarefa2 = new TarefaSimples("Preparar o relatório");
        tarefa3 = new TarefaSimples("Enviar e-mails");

        tarefaComposta1 = new TarefaComposta("Preparar a reunião");
        tarefaComposta1.adicionarTarefa(tarefa1);
        tarefaComposta1.adicionarTarefa(tarefa2);

        tarefaComposta2 = new TarefaComposta("Projeto X");
        tarefaComposta2.adicionarTarefa(tarefaComposta1);
        tarefaComposta2.adicionarTarefa(tarefa3);
    }

    @Test
    public void testRealizarTarefaSimples() {
        tarefa1.realizar();
        tarefa2.realizar();
        tarefa3.realizar();
    }

    @Test
    public void testRealizarTarefaComposta1() {
        tarefaComposta1.realizar();
    }

    @Test
    public void testRealizarTarefaComposta2() {
        tarefaComposta2.realizar();
    }

    @Test
    public void testToStringTarefaSimples() {
        assertEquals("Comprar materiais", tarefa1.toString());
    }

    @Test
    public void testToStringTarefaComposta() {
        assertEquals("Preparar a reunião com sub-tarefas: [Comprar materiais, Preparar o relatório]", tarefaComposta1.toString());
    }
}
