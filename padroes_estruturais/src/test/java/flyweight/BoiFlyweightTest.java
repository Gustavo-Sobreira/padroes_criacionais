package flyweight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BoiFlyweightTest {

    @Test
    public void testCriarBoi() {
        Boi boi1 = BoiFactory.getBoi("Nelore", "Branco");
        assertNotNull(boi1);
        assertTrue(boi1 instanceof BoiConcreto);
    }

    @Test
    public void testObterDetalhesBoi() {
        Boi boi1 = BoiFactory.getBoi("Nelore", "Branco");
        Boi boi2 = BoiFactory.getBoi("Angus", "Preto");

        assertEquals("Nelore, cor: Branco", ((BoiConcreto) boi1).getRaca() + ", cor: " + ((BoiConcreto) boi1).getCor());
        assertEquals("Angus, cor: Preto", ((BoiConcreto) boi2).getRaca() + ", cor: " + ((BoiConcreto) boi2).getCor());
    }

    @Test
    public void testReutilizacaoBoi() {
        Boi boi1 = BoiFactory.getBoi("Nelore", "Branco");
        Boi boi2 = BoiFactory.getBoi("Angus", "Preto");
        Boi boi3 = BoiFactory.getBoi("Nelore", "Branco");
        Boi boi4 = BoiFactory.getBoi("Hereford", "Marrom");
        Boi boi5 = BoiFactory.getBoi("Angus", "Preto");

        assertSame(boi1, boi3);
        assertSame(boi2, boi5);
        assertNotSame(boi1, boi2);
    }

    @Test
    public void testTotalBois() {
        BoiFactory.getBoi("Nelore", "Branco");
        BoiFactory.getBoi("Angus", "Preto");
        BoiFactory.getBoi("Nelore", "Branco");
        BoiFactory.getBoi("Hereford", "Marrom");
        BoiFactory.getBoi("Angus", "Preto");

        assertEquals(3, BoiFactory.getTotalBois());
    }
}
