package facade;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class AlexaTest {

    @Test
    public void testLigarCasa() {
        assertTrue(ControleDispositivosFacade.ligarCasa());
    }

    @Test
    public void testDesligarCasa() {
        assertFalse(ControleDispositivosFacade.desligarCasa());
    }
}
