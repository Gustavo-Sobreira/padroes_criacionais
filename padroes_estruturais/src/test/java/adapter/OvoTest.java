package adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OvoTest {

    private Ovo ovo;
    private IOvoMedida ovoMedida;
    private OvoAdpter ovoAdpter;

    @BeforeEach
    public void setUp() {
        ovo = new Ovo();
        ovoMedida = new OvoMedida();
        ovoAdpter = new OvoAdpter(ovoMedida);
    }

    @Test
    public void testSetMedidaGrande() {
        ovo.setMedida("Grande");
        assertEquals("Grande", ovo.getMedida());
        assertEquals(1000, ovo.getPeso());
    }

    @Test
    public void testSetMedidaMedio() {
        ovo.setMedida("Médio");
        assertEquals("Médio", ovo.getMedida());
        assertEquals(500, ovo.getPeso());
    }

    @Test
    public void testSetMedidaPequeno() {
        ovo.setMedida("Pequeno");
        assertEquals("Pequeno", ovo.getMedida());
        assertEquals(250, ovo.getPeso());
    }

    @Test
    public void testRecuperarMedidaGrande() {
        ovoAdpter.setPesoEmGrama(1000);
        assertEquals("Grande", ovoAdpter.recuperarMedida());
    }

    @Test
    public void testRecuperarMedidaMedio() {
        ovoAdpter.setPesoEmGrama(500);
        assertEquals("Médio", ovoAdpter.recuperarMedida());
    }

    @Test
    public void testRecuperarMedidaPequeno() {
        ovoAdpter.setPesoEmGrama(250);
        assertEquals("Pequeno", ovoAdpter.recuperarMedida());
    }

    @Test
    public void testSalvarMedidaGrande() {
        ovoMedida.setMedida("Grande");
        ovoAdpter.salvarMEdida();
        assertEquals(1000, ovoAdpter.getPesoEmGrama());
    }

    @Test
    public void testSalvarMedidaMedio() {
        ovoMedida.setMedida("Médio");
        ovoAdpter.salvarMEdida();
        assertEquals(500, ovoAdpter.getPesoEmGrama());
    }

    @Test
    public void testSalvarMedidaPequeno() {
        ovoMedida.setMedida("Pequeno");
        ovoAdpter.salvarMEdida();
        assertEquals(250, ovoAdpter.getPesoEmGrama());
    }
}
